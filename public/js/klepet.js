// V tej datoteki je opredeljen objekt Klepet, 
// ki nam olajša obvladovanje funkcionalnosti uporabnika

// slike * smejkoti
// zasebna slika!
var Klepet = function(socket) {
  this.socket = socket;
};

var imena=[];
var preimenovanja=[];

Klepet.prototype.preimenuj = function (ime){
  var a = imena.indexOf(ime);
   if (a != -1){
    
     ime= preimenovanja[a] + " ("+imena[a]+")";
    
    }
    
   return ime;
}
Klepet.prototype.getIme = function(vzdevek){
  var a =vzdevek.split('(')[0];
     a = a.substring(0,a.length-1);
     a=preimenovanja.indexOf(a);
  if(a!=-1)  {
    return imena[a];
  }
  else {
   
    return vzdevek; 
  }
}
function pridobiIme(vzdevek){
  
  var a = imena.indexOf(vzdevek)
  
  if(a != -1){
    return preimenovanja[a]
  }
  else return vzdevek
}
/**
 * Pošiljanje sporočila od uporabnika na strežnik z uporabo 
 * WebSocket tehnologije po socketu 'sporocilo'
 * 
 * @param kanal ime kanala, na katerega želimo poslati sporočilo
 * @param besedilo sporočilo, ki ga želimo posredovati
 */
Klepet.prototype.posljiSporocilo = function(kanal, besedilo) {
  var sporocilo = {
    kanal: kanal,
    besedilo: besedilo
  };
  this.socket.emit('sporocilo', sporocilo);
};


/**
 * Uporabnik želi spremeniti kanal, kjer se zahteva posreduje na strežnik 
 * z uporabo WebSocket tehnologije po socketu 'pridruzitevZahteva'
 * 
 * @param kanal ime kanala, kateremu se želimo pridružiti
 */
Klepet.prototype.spremeniKanal = function(kanal) {
  this.socket.emit('pridruzitevZahteva', {
    novKanal: kanal
  });
};


/**
 * Procesiranje ukaza, ki ga uporabnik vnese v vnosno polje, kjer je potrebno
 * najprej izluščiti za kateri ukaz gre, nato pa prebrati še parametre ukaza
 * in zahtevati izvajanje ustrezne funkcionalnosti
 * 
 * @param ukaz besedilo, ki ga uporabnik vnese v vnosni obrazec na spletu
 */
Klepet.prototype.procesirajUkaz = function(ukaz) {
  var besede = ukaz.split(' ');
  // Izlušči ukaz iz vnosnega besedila
  ukaz = besede[0].substring(1, besede[0].length).toLowerCase();
  var sporocilo = false;

  switch(ukaz) {
    case 'preimenuj':
       console.log("preimenovan")
       
    
    besede.shift();
    var besedilo = besede.join(' ');
    
    
      var parametri = besedilo.split('\"');
      console.log(parametri)
      if (parametri) {
      
      
       var ime = parametri[1];
       var menjava = parametri[3];
       var a = imena.indexOf(ime);
      
       if( a != -1){
         preimenovanja[a] = menjava;
       }
       else{
         var dolzina = imena.length;
         imena[dolzina] = ime;
         preimenovanja[dolzina] = menjava;
         dolzina = imena.length;
        
       }
       
      }
      
    break;
    case 'pridruzitev':
      besede.shift();
      var kanal = besede.join(' ');
      // Sproži spremembo kanala
      this.spremeniKanal(kanal);
      break;
    case 'vzdevek':
      besede.shift();
      var vzdevek = besede.join(' ');
      // Zahtevaj spremembo vzdevka
      this.socket.emit('vzdevekSpremembaZahteva', vzdevek);
      break;
    case 'zasebno':
      
      var besedilo = besede.join(' ');
      var parametri = besedilo.split('\"');
      if (parametri) {
        ime = pridobiIme(parametri[1]);
        
        if (ime === parametri[1]){
          var komu = parametri[1];
          
        }
        else {
          var komu =  ime+ '('+parametri[1]+')'
        }
        this.socket.emit('sporocilo', {vzdevek: parametri[1], besedilo: parametri[3]});
        sporocilo = '(zasebno za ' + komu + '): ' + parametri[3];
      } else {
        sporocilo = 'Neznan ukaz';
      }
      break;
     case 'barva':
       besede.shift();
       var vzdevek = besede.join(' ');
      
       changecolor(vzdevek);
       sporocilo = "";
       break;
    default:
      // Vrni napako, če pride do neznanega ukaza
      sporocilo = 'Neznan ukaz.';
      break;
  };

  return sporocilo;
};

/**Sprememba barve
 * 
 */
  function changecolor(color){
    document.querySelector('#kanal').style.background = color;
  }